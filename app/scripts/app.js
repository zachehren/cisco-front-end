(function() {
  function config($locationProvider, $stateProvider) {

    $locationProvider
      .html5Mode({
        enabled: true,
        requireBase: false
      });

    $stateProvider
      .state('listDomains', {
        url: '/',
        controller: 'ListDomainsCtrl as listDomains',
        templateUrl: '/templates/listDomains.html'
      })
      .state('submitDomain', {
        url: '/submit-domain',
        controller: 'SubmitDomainCtrl as submitDomain',
        templateUrl: '/templates/submitDomain.html'
      });
  }

  angular
    .module('ciscoFrontEnd', ['ui.router', 'ui.bootstrap', 'ngRoute'])
    .config(config);
})();
