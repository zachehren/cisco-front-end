(function() {
  function DomainFactory($http) {

    var DomainFactory = {};

    DomainFactory.listDomains = function(){
      var listDomains = {
        method: 'GET',
        url: 'http://localhost:3000/domain_info'
      };

      return $http(listDomains).then(function successCallback(response) {
        DomainFactory.allValidDomains = response.data;
      });
    };


    DomainFactory.submitDomain = function(domain, description) {
      var submitDomain = {
        method: 'POST',
        url: 'http://localhost:3000/domain_info',
        data: {
          domain_name: domain,
          domain_description: description
        }
      }

      $http(submitDomain).then(function successCallback(response) {
        DomainFactory.newDomain = response.data;
        DomainFactory.listDomains();
      });
    };

    return DomainFactory;
  };

  angular
    .module('ciscoFrontEnd')
    .factory('DomainFactory', ['$http', DomainFactory])
})();
