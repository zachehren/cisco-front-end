(function(){
  function ListDomainsCtrl(DomainFactory) {

    DomainFactory.listDomains();
    
    this.DomainFactory = DomainFactory;
  }

  angular
    .module('ciscoFrontEnd')
    .controller('ListDomainsCtrl', ['DomainFactory', ListDomainsCtrl]);
})();
