(function() {
  function SubmitDomainCtrl(DomainFactory, $location) {

    this.submit = function() {
      strippedDomain = String(this.domain).replace(/^(https?:\/\/)?(www\.)?()/,'');
      strippedDomain = strippedDomain.split("/");
      DomainFactory.submitDomain(strippedDomain[0], this.description);
      $location.path('/');
    };

  }

  angular
    .module('ciscoFrontEnd')
    .controller('SubmitDomainCtrl', ['DomainFactory', '$location', SubmitDomainCtrl]);
})();
