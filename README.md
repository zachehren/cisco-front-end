## Cisco Front-End

* View valid domains plus their description and submission date
* Submit domains to MySQL database

*Used in conjunction with cisco-api on the back-end (https://bitbucket.org/zachehren/cisco-api/src/master/)*

### Configuration

Install the project dependencies by running:

```
$ npm install
```

### Run the Application

Run the application server:

```
$ npm start
```

Navigate to `http://localhost:3001` to view locally.
